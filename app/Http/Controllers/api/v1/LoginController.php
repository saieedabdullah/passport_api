<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\user;

class LoginController extends Controller
{
  public function index()
  {
      $data = User::all();
      if ( !$data ) {
        return response(['message' => 'Invalid login Data']);
      }
      else {
        return $data;
      }

  }
  public function login(Request $request){
      $login = $request->validate([
        'email' => 'required|string',
        'password' => 'required|string'
      ]);

      if( !Auth::attempt( $login ) ){
        return response(['message' => 'Invalid login Data']);
      }

      $accessToken = Auth::user()->createToken('authToken')->accessToken;
      return response(['user' => Auth::user(), 'access_token' => $accessToken]);
    }

    public function logout (Request $request) {

      $token = $request->user()->token();
      $singOut = $token->revoke();
        $response = 'You have been succesfully logged out!';
        return response($response, 200);
      if (!AUTH::check) {
        $response = 'error';
        return response($response, 404);
      }
      
  }
}
